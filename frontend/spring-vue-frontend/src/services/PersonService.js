/* import axios from 'axios'

const Person_Api_Base_url = 'http://localhost:8082/api/persons'

class PersonService{
    getPersons(){
        return axios.get(Person_Api_Base_url);
    }
}

export default new PersonService()

 */

import http from "../http-common"

class PersonService{
    getPersons(){
        console.log("dfdfd")
        return http.get('/persons')

    }
    get(id) {
        return http.get(`/persons/${id}`)
    }

    create(data) {
        return http.post('/persons', data)
    }

    update(id, data) {
        return http.put(`/persons/${id}`, data)
    }

    delete(id) {
        return http.delete(`/persons/${id}`)
    }
}

export default new PersonService()




