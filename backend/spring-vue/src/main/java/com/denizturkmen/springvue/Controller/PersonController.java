package com.denizturkmen.springvue.Controller;


import com.denizturkmen.springvue.Exception.ResourceNotFoundException;
import com.denizturkmen.springvue.Model.Person;
import com.denizturkmen.springvue.Repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @GetMapping(value = "/persons")
    public List<Person> getAllPerson(){
        return personRepository.findAll();
    }
    @PostMapping(value = "/persons")
    public Person createPerson(@Validated @RequestBody Person person){
        return personRepository.save(person);
    }

    @GetMapping("/persons/{id}")
    public Person getPersonById(@PathVariable(value = "id") Long personId){
        return personRepository.findById(personId)
                .orElseThrow(()-> new ResourceNotFoundException("Person","id",personId));
    }

    @PutMapping("/persons/{id}")
    public Person updateNote(@PathVariable(value = "id") Long personId,
                           @Validated @RequestBody Person personDetails) {

        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new ResourceNotFoundException("Person", "id", personId));

        person.setAdi(personDetails.getAdi());
        person.setSoyadi(personDetails.getSoyadi());

        Person updatedPerson = personRepository.save(person);
        return updatedPerson;
    }

    @DeleteMapping("/persons/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long personId) {
        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new ResourceNotFoundException("Person", "id", personId));

        personRepository.delete(person);

        return ResponseEntity.ok().build();
    }

}
